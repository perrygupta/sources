//
// Generated by SwagGen
// https://github.com/yonaskolb/SwagGen
//

import Foundation

public class CreateOrder: APIModel {

    public enum `Type`: String, Codable, Equatable, CaseIterable {
        case buyThenSell = "BUY_THEN_SELL"
        case sell = "SELL"
    }

    public var followUpwards: Bool?

    public var limitLoss: Bool?

    public var lossLimitPercentage: Double?

    public var lossLimitPrice: Double?

    public var profitPercentage: Double?

    public var profitPrice: Double?

    public var symbol: String?

    public var targetAmount: Double?

    public var targetBuyPrice: Double?

    public var type: `Type`?

    public init(followUpwards: Bool? = nil, limitLoss: Bool? = nil, lossLimitPercentage: Double? = nil, lossLimitPrice: Double? = nil, profitPercentage: Double? = nil, profitPrice: Double? = nil, symbol: String? = nil, targetAmount: Double? = nil, targetBuyPrice: Double? = nil, type: `Type`? = nil) {
        self.followUpwards = followUpwards
        self.limitLoss = limitLoss
        self.lossLimitPercentage = lossLimitPercentage
        self.lossLimitPrice = lossLimitPrice
        self.profitPercentage = profitPercentage
        self.profitPrice = profitPrice
        self.symbol = symbol
        self.targetAmount = targetAmount
        self.targetBuyPrice = targetBuyPrice
        self.type = type
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        followUpwards = try container.decodeIfPresent("followUpwards")
        limitLoss = try container.decodeIfPresent("limitLoss")
        lossLimitPercentage = try container.decodeIfPresent("lossLimitPercentage")
        lossLimitPrice = try container.decodeIfPresent("lossLimitPrice")
        profitPercentage = try container.decodeIfPresent("profitPercentage")
        profitPrice = try container.decodeIfPresent("profitPrice")
        symbol = try container.decodeIfPresent("symbol")
        targetAmount = try container.decodeIfPresent("targetAmount")
        targetBuyPrice = try container.decodeIfPresent("targetBuyPrice")
        type = try container.decodeIfPresent("type")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encodeIfPresent(followUpwards, forKey: "followUpwards")
        try container.encodeIfPresent(limitLoss, forKey: "limitLoss")
        try container.encodeIfPresent(lossLimitPercentage, forKey: "lossLimitPercentage")
        try container.encodeIfPresent(lossLimitPrice, forKey: "lossLimitPrice")
        try container.encodeIfPresent(profitPercentage, forKey: "profitPercentage")
        try container.encodeIfPresent(profitPrice, forKey: "profitPrice")
        try container.encodeIfPresent(symbol, forKey: "symbol")
        try container.encodeIfPresent(targetAmount, forKey: "targetAmount")
        try container.encodeIfPresent(targetBuyPrice, forKey: "targetBuyPrice")
        try container.encodeIfPresent(type, forKey: "type")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? CreateOrder else { return false }
      guard self.followUpwards == object.followUpwards else { return false }
      guard self.limitLoss == object.limitLoss else { return false }
      guard self.lossLimitPercentage == object.lossLimitPercentage else { return false }
      guard self.lossLimitPrice == object.lossLimitPrice else { return false }
      guard self.profitPercentage == object.profitPercentage else { return false }
      guard self.profitPrice == object.profitPrice else { return false }
      guard self.symbol == object.symbol else { return false }
      guard self.targetAmount == object.targetAmount else { return false }
      guard self.targetBuyPrice == object.targetBuyPrice else { return false }
      guard self.type == object.type else { return false }
      return true
    }

    public static func == (lhs: CreateOrder, rhs: CreateOrder) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
