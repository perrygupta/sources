//
// Generated by SwagGen
// https://github.com/yonaskolb/SwagGen
//

import Foundation

public class UpdateCredit: APIModel {

    public enum `Type`: String, Codable, Equatable, CaseIterable {
        case newAccountBonus = "NewAccountBonus"
        case goodwill = "Goodwill"
        case webPurchase = "WebPurchase"
        case iAPPurchase = "IAPPurchase"
        case referralBonus = "ReferralBonus"
        case orderConsumed = "OrderConsumed"
        case orderCancelled = "OrderCancelled"
        case orderLocked = "OrderLocked"
    }

    public var amount: Int?

    /** Any additional comments */
    public var comments: String?

    /** Unique identifier of the source that cause the update. e.g. Order Id, IAP Id, Referred User's Id */
    public var source: String?

    public var type: `Type`?

    public init(amount: Int? = nil, comments: String? = nil, source: String? = nil, type: `Type`? = nil) {
        self.amount = amount
        self.comments = comments
        self.source = source
        self.type = type
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: StringCodingKey.self)

        amount = try container.decodeIfPresent("amount")
        comments = try container.decodeIfPresent("comments")
        source = try container.decodeIfPresent("source")
        type = try container.decodeIfPresent("type")
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: StringCodingKey.self)

        try container.encodeIfPresent(amount, forKey: "amount")
        try container.encodeIfPresent(comments, forKey: "comments")
        try container.encodeIfPresent(source, forKey: "source")
        try container.encodeIfPresent(type, forKey: "type")
    }

    public func isEqual(to object: Any?) -> Bool {
      guard let object = object as? UpdateCredit else { return false }
      guard self.amount == object.amount else { return false }
      guard self.comments == object.comments else { return false }
      guard self.source == object.source else { return false }
      guard self.type == object.type else { return false }
      return true
    }

    public static func == (lhs: UpdateCredit, rhs: UpdateCredit) -> Bool {
        return lhs.isEqual(to: rhs)
    }
}
